# WordPress test imports

Use these XML files to test your importation of WordPress.

For more information see

* https://codex.wordpress.org/Theme_Unit_Test
* https://github.com/Evinceknow/Theme-Unit-Test-Environment-for-WordPress
* https://codex.wordpress.org/Theme_Development#Theme_Testing_Process

## Other tests

* http://wptest.io/
* https://github.com/poststatus/wptest

## Source of files:

* #4: https://raw.githubusercontent.com/poststatus/wptest/master/wptest.xml
* #7: https://github.com/wpaccessibility/a11y-theme-unit-test/blob/master/a11y-theme-unit-test-data.xml
* #8: https://github.com/WordPress/theme-test-data/blob/master/theme-preview.xml
* #9: https://github.com/jawordpressorg/theme-test-data-ja/blob/master/wordpress-theme-test-data-ja.xml
* #10: https://github.com/WordPress/theme-test-data/blob/master/64-block-test-data.xml

## Generate dummy code

* https://wordpress.org/plugins/fakerpress/

## More places to get WordPress demo content

* https://www.wpzoom.com/theme-demo-content/
* https://www.theme-junkie.com/demo-content/
* https://www.designlabthemes.com/import-demo-content-wordpress/

## Maintainer

* Developed by [HongPong](https://github.com/HongPong) mainly to support Drupal development of [wordpress_migrate](https://www.drupal.org/project/wordpress_migrate) 